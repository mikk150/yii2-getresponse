Yii2 getresponse.com api client component
===========================

**Master:** 

![build](https://gitlab.com/nuffic/yii2-getresponse/badges/master/build.svg) ![coverage](https://gitlab.com/nuffic/yii2-getresponse/badges/master/coverage.svg)

**Develop:** 

![build](https://gitlab.com/nuffic/yii2-getresponse/badges/develop/build.svg) ![coverage](https://gitlab.com/nuffic/yii2-getresponse/badges/develop/coverage.svg)

Installation:
-----

Via composer:
`composer.phar require "nuffic/yii2-getresponse":"~1.2.0"`

Usage:
-----

```php
$client = new \nuffic\getresponse\Client([
   'apiKey' => 'API_KEY',
   'domain' => 'example.org', # domain for enterprise users
]);

$client->addContact('asd123', 'user@email.com');
```

Available methods:
-----------------

```php
searchContact('user@email.com'); // Gets single contact ID
```

```php
searchContactInAllLists('user@email.com'); // Gets an array of contact IDs
```

```php
addContact('asd123', 'user@email.com', ['qwer12' => 'super awesome value']); // campaign ID, email, custom fields
```

```php
updateCustomFields('asd123', ['qwer12' => 'super awesome value']); // contact ID, custom fields
```

```php
deleteContact('asd123'); // Removes contact by ID
```
