<?php

namespace nuffic\getresponse\exceptions;

use yii\base\Exception;

/**
 * Class BlockedContactException
 *
 * @package nuffic\getresponse\exceptions
 */
class BlockedContactException extends Exception
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Blocked Contact';
    }
}
