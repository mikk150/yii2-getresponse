<?php

namespace nuffic\getresponse;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\ClientInterface as HttpClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use nuffic\getresponse\exceptions\BlacklistedContactException;
use nuffic\getresponse\exceptions\BlockedContactException;
use nuffic\getresponse\exceptions\ClientException;
use nuffic\getresponse\exceptions\ContactNotFoundException;
use nuffic\getresponse\exceptions\DeletedContactException;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class Client
 * @package nuffic\getresponse
 */
class Client extends Component
{
    /**
     * @var string
     */
    public $baseUrl = 'https://api.getresponse.com/v3/';

    /**
     * @var string
     */
    public $apiKey;

    /**
     * @var string|array|HttpClientInterface
     */
    public $httpClient;

    /**
     * @var string domain for enterprise users (if empty, no X-Domain header is sent)
     */
    public $domain;

    const BLACKLISTED_CONTACT_MESSAGE = 'Cannot add contact that is blacklisted';
    const CONTACT_NOT_FOUND_MESSAGE = 'Resource not found';
    const DELETED_CONTACT_MESSAGE = 'Contact deleted';
    const BLOCKED_CONTACT_MESSAGE = 'Cannot add contact that is blocked';

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if ($this->apiKey === null) {
            throw new InvalidConfigException('`apiKey` is required');
        }

        $headers = ['X-Auth-Token' => 'api-key ' . $this->apiKey];
        if ($this->domain) {
            $headers['X-Domain'] = $this->domain;
        }

        if ($this->httpClient === null) {
            $this->httpClient = new HttpClient([
                'base_uri' => $this->baseUrl,
                'headers' => $headers,
            ]);
        }
    }

    /**
     * @param $email
     * @return string|null contact ID
     * @throws InvalidConfigException
     */
    public function searchContact($email)
    {
        return ArrayHelper::getValue($this->getContactsByEmail($email), '0.contactId');
    }

    /**
     * @param string $email
     * @return array
     */
    public function searchContactInAllLists($email)
    {
        return array_map(function ($record) {
            return ArrayHelper::getValue($record, 'contactId');
        }, $this->getContactsByEmail($email));
    }

    /**
     * @param $campaignID
     * @param string $email Customer email
     * @param array $params custom fields in format `fieldId => value`
     * @return boolean whether request was successful
     * @throws ClientException
     * @throws BlacklistedContactException
     */
    public function addContact($campaignID, $email, $params = [])
    {
        try {
            $response = $this->getClient()->post('contacts', [
                'json' => [
                    'email' => $email,
                    'campaign' => ['campaignId' => $campaignID],
                    'customFieldValues' => $this->mapCustomFields($params),
                ]
            ]);

            return $response->getStatusCode() == 202;
        } catch (BadResponseException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            $decodedResponse = Json::decode($response);
            if (ArrayHelper::getValue($decodedResponse, 'message') === self::BLACKLISTED_CONTACT_MESSAGE) {
                throw new BlacklistedContactException;
            }

            if (ArrayHelper::getValue($decodedResponse, 'message') === self::BLOCKED_CONTACT_MESSAGE) {
                throw new BlockedContactException;
            }

            if (ArrayHelper::getValue($decodedResponse, 'message') === self::DELETED_CONTACT_MESSAGE) {
                throw new DeletedContactException;
            }
            throw new ClientException($response, $e->getCode(), $e);
        }
    }

    /**
     * @param $contactID
     * @param $params
     * @return bool
     * @throws InvalidConfigException
     */
    public function updateCustomFields($contactID, $params)
    {
        $response = $this->getClient()->post(strtr('contacts/{contact_id}/custom-fields', ['{contact_id}' => $contactID]), [
            'json' => [
                'customFieldValues' => $this->mapCustomFields($params),
            ]
        ]);

        return $response->getStatusCode() == 200;
    }

    /**
     * @param string $contactID
     * @return bool
     * @throws InvalidConfigException
     * @throws ContactNotFoundException
     */
    public function deleteContact($contactID)
    {
        try {
            $response = $this->getClient()->delete(strtr('contacts/{contact_id}', [
                '{contact_id}' => $contactID
            ]));
            return $response->getStatusCode() == 204;
        } catch (BadResponseException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            if (ArrayHelper::getValue(Json::decode($response), 'message') == self::CONTACT_NOT_FOUND_MESSAGE) {
                throw new ContactNotFoundException;
            }
            throw new ClientException($response, $e->getCode(), $e);
        }
    }

    protected function mapCustomFields($params)
    {
        $out = [];
        foreach ($params as $key => $param) {
            if ($param === null) {
                continue;
            }
            $out[] = ['customFieldId' => $key, 'value' => [$param]];
        }

        return $out;
    }

    /**
     * @return HttpClient
     * @throws InvalidConfigException
     */
    private function getClient()
    {
        return Instance::ensure($this->httpClient, HttpClient::class);
    }

    /**
     * @param $email
     * @return array
     * @throws InvalidConfigException
     */
    private function getContactsByEmail($email)
    {
        $response = $this->getClient()->get(
            strtr('contacts?query[email]={email}&fields=email', ['{email}' => $email])
        );
        return Json::decode($response->getBody()->getContents());
    }
}
